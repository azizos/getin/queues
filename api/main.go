package main

import (
	"context"
	"fmt"
	"os"

	"dev.azure.com/happyq/HappyQ/_git/queues/api/common"
	api "dev.azure.com/happyq/HappyQ/_git/queues/api/web"
	"github.com/ardanlabs/conf"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
)

var dbCollectionName string = "queues"
var log, _ = common.GetLogModule()
var db *mongo.Database
var ctx context.Context

func main() {
	if err := run(); err != nil {
		log.Error("Service cannot be started: ", err)
		os.Exit(1)
	}
}

func run() error {
	var cfg struct {
		MongoURI      string `conf:"default:mongodb://localhost:27017"`
		MongoDB       string `conf:"default:tatalib"`
		APIHost       string `conf:"default:0.0.0.0:8080"`
		GinMode       string `conf:"default:debug"`
		RedisURI      string `conf:"default:localhost:6379"`
		RedisPassword string `conf:""`
	}

	if err := conf.Parse(os.Args[1:], "queues", &cfg); err != nil {
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("queues", &cfg)
			if err != nil {
				log.Error("Generating config usage: ", err)
			}
			fmt.Println(usage)
			return nil
		}
		return errors.Wrap(err, "Problem parsing api configs")
	}

	defer log.Info("Main: completed")

	redis := common.ConnectToRedis(cfg.RedisURI, cfg.RedisPassword)

	db = common.ConnectToMongo(cfg.MongoURI, cfg.MongoDB)
	mongo := db.Collection(dbCollectionName)

	gin.SetMode(cfg.GinMode)
	router := gin.Default()
	api.GetRoutes(router, redis, mongo)

	err := router.Run(cfg.APIHost)
	return err
}
