package models

// Queue struct is used to store and parse queue objects
type Queue struct {
	ID           string   `bson:"_id,omitempty" json:"_id,omitempty"`
	BusinessID   string   `bson:"business_id,omitempty" json:"business_id,omitempty"`
	Entries      []string `bson:"entries,omitempty" json:"entries,omitempty"`
	EntriesCount int      `bson:"entries_count,omitempty" json:"entries_count,omitempty"`
}

// QueueEntry is used to store the user ID for each visitor that's waiting at the moment
type QueueEntry struct {
	QueueID   string `json:"queue_id"`
	VisitorID string `json:"visitor_id"`
}
