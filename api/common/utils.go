package common

import (
	"context"

	"github.com/go-redis/redis"
	"github.com/op/go-logging"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// GetLogModule is used to create and return a formatted logging module to be used globally in app
func GetLogModule() (*logging.Logger, error) {
	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.6s} %{id:03x}%{color:reset} %{message}`,
	)
	logging.SetFormatter(format)
	return logging.GetLogger("app")
}

var log, _ = GetLogModule()

// ConnectToMongo makes connection with a mongo database (to manage queues)
func ConnectToMongo(mongoURI string, mongoDB string) *mongo.Database {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Panicf("Error connecting to database: %v", err)
	}
	database := client.Database(mongoDB)
	log.Info("Connected to Mongo server")
	return database
}

// ConnectToRedis makes connection with Redis (to manage queue entries)
func ConnectToRedis(uri string, password string) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     uri,
		Password: password,
		DB:       0,
	})
	pong, err := client.Ping().Result()
	if err != nil {
		log.Panicf("Error connecting to Redis database: %v", err)
	}
	log.Info("Connected to Redis server: ", pong)
	return client
}
