package services

import (
	"context"
	"time"

	"dev.azure.com/happyq/HappyQ/_git/queues/api/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

var ctx context.Context
var ctxCancelFunc context.CancelFunc
var timeTillContextDeadline = 2 * time.Second

// ListQueues retrieves all queue objects from the database
func ListQueues(collection *mongo.Collection) []*models.Queue {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var queues []*models.Queue
	cursor, err := collection.Find(ctx, bson.M{})
	defer cursor.Close(ctx)
	if err != nil {
		log.Debug("No queues returned from the database:", err)
	} else {
		for cursor.Next(context.TODO()) {
			var result *models.Queue
			err := cursor.Decode(&result)
			if err != nil {
				log.Debug("Problem retrieving queue object:", err)
			} else {
				queues = append(queues, result)
			}
		}
	}
	return queues
}

// StoreQueue stores a new queue
func StoreQueue(collection *mongo.Collection, newQueueObject *models.Queue) *models.Queue {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *models.Queue
	res, err := collection.InsertOne(ctx, newQueueObject)
	if err != nil {
		log.Errorf("Problem updating a queue: %v: %v", err)
	}
	err = collection.FindOne(ctx, bson.M{"_id": res.InsertedID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a queue: %v", err)
	}
	log.Infof("queue %v has been added", res.InsertedID)
	return result
}

// GetQueue finds one single queue object based on id
func GetQueue(collection *mongo.Collection, id string, entries []string) *models.Queue {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *models.Queue
	objectID, _ := primitive.ObjectIDFromHex(id)
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	result.EntriesCount = len(entries)
	result.Entries = entries
	if err != nil {
		log.Errorf("Problem finding a queue: %v", err)
	}
	return result
}

// DeleteQueue delete one single queue object based on id
func DeleteQueue(collection *mongo.Collection, id string) error {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)
	res := collection.FindOneAndDelete(ctx, bson.M{"_id": objectID})
	if res.Err() != nil {
		log.Errorf("Problem removing a queue: %v: %v", objectID, res.Err)
	}
	return res.Err()
}

// QueueExists checks if a queue already exists
func QueueExists(collection *mongo.Collection, id string) bool {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *mongo.SingleResult
	objectID, _ := primitive.ObjectIDFromHex(id)

	result = collection.FindOne(ctx, bson.M{"_id": objectID})
	if result.Err() == nil {
		return true
	}
	return false
}
