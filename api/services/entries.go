package services

import (
	"dev.azure.com/happyq/HappyQ/_git/queues/api/common"
	"dev.azure.com/happyq/HappyQ/_git/queues/api/models"

	"github.com/go-redis/redis"
)

var log, _ = common.GetLogModule()

// GetAllEntries returns all elements in a queue list
func GetAllEntries(db *redis.Client, queueID string) []string {
	if keyExists(db, queueID) == true {
		result, err := db.ZRangeByScore(queueID, redis.ZRangeBy{Min: "-inf", Max: "+inf"}).Result()
		if err != nil {
			log.Errorf("Error performing (ZRANGEBYSCORE) on key %v", queueID)
		}
		return result
	}
	return nil
}

// GetQueueSize returns the length of a queue(
func GetQueueSize(db *redis.Client, queueID string) int64 {
	result, err := db.ZCard(queueID).Result()
	if err != nil {
		log.Errorf("Error performing (ZCard) on key %v", queueID)
	}
	return result
}

// JoinQueue is used to join a queue
func JoinQueue(db *redis.Client, queueItem *models.QueueEntry, score float64) bool {
	result, err := db.ZAdd(queueItem.QueueID, redis.Z{Score: score, Member: queueItem.VisitorID}).Result()
	if err != nil {
		log.Errorf("Error performing (ZAdd) on key %v", queueItem.QueueID)
		return false
	}
	if result == 0 {
		log.Errorf("Visitor '%v' already exists in queue '%v'", queueItem.VisitorID, queueItem.QueueID)
		return false
	}
	log.Infof("Visitor '%v' joined queue '%v'", queueItem.VisitorID, queueItem.QueueID)
	return true
}

// ExitQueue is used to leave a queue
func ExitQueue(db *redis.Client, queueItem *models.QueueEntry) bool {
	if keyExists(db, queueItem.QueueID) == true {
		_, err := db.ZRem(queueItem.QueueID, queueItem.VisitorID).Result()
		if err != nil {
			log.Errorf("Error performing (ZRem) on key %v", queueItem.QueueID)
		}
		log.Infof("Visitor '%v' left queue '%v'", queueItem.VisitorID, queueItem.QueueID)

		return true
	}
	return false
}

// GetRankingInQueue returns the curent index of a queue entry
func GetRankingInQueue(db *redis.Client, queueItem *models.QueueEntry) (int64, error) {
	result, err := db.ZRank(queueItem.QueueID, queueItem.VisitorID).Result()
	if err != nil {
		log.Errorf("Error performing (ZRank) on key '%v'", queueItem.QueueID)
	}
	log.Infof("Visitor '%v' has position '%v' in the queue '%v'", queueItem.VisitorID, result, queueItem.QueueID)
	return result, err
}

func deleteFirstInQueue(db *redis.Client, queueID string) []redis.Z {
	result, err := db.ZPopMin(queueID).Result()
	if err != nil {
		log.Errorf("Error performing (ZPopMin) on key %v", queueID)
	}
	deletedVisitorID := result[0].Member
	log.Infof("Visitor '%v' has been removed from the queue '%v'", deletedVisitorID, queueID)
	return result
}

func keyExists(db *redis.Client, key string) bool {
	result, err := db.Exists(key).Result()
	if err != nil {
		log.Errorf("Error performing (Exists) on key '%v'", key)
	}
	if result == 0 {
		log.Errorf("Element '%v' cannot be found", key)
		return false
	}
	return true
}
