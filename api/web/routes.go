package api

import (
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"go.mongodb.org/mongo-driver/mongo"
)

// GetRoutes defines and returns a list of routes to be used by the main HTTP handler
func GetRoutes(route *gin.Engine, redis *redis.Client, mongo *mongo.Collection) {
	check := Check{}

	route.Use(gin.Recovery())
	route.Use(dbMiddleware(redis, mongo))

	queues := route.Group("/queues")
	{
		queues.GET("/", GetQueues)
		queues.POST("/", StoreQueue)
		queues.GET("/:queueId/", GetQueue)
		queues.DELETE("/:queueId/", DeleteQueue)
	}
	queueEntries := route.Group("/entries")
	{
		queueEntries.GET("/:queueId/", GetAllEntries)
		queueEntries.POST("/", JoinQueue)
		queueEntries.DELETE("/", ExitQueue)
		queueEntries.GET("/:queueId/count", GetQueueSize)
		queueEntries.GET("/:queueId/visitors/:visitorId", GetRankingInQueue)
	}

	route.GET("/health", check.Health)
}

func dbMiddleware(redis *redis.Client, mongo *mongo.Collection) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("redis", redis)
		c.Set("mongo", mongo)
		c.Next()
	}
}
