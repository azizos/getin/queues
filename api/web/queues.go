package api

import (
	"dev.azure.com/happyq/HappyQ/_git/queues/api/models"
	"dev.azure.com/happyq/HappyQ/_git/queues/api/services"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

// GetQueues is used to retrieve a list of queue objects
func GetQueues(context *gin.Context) {
	db := context.MustGet("mongo").(*mongo.Collection)
	queues := services.ListQueues(db)

	if len(queues) <= 0 {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, queues)
	return
}

// StoreQueue is used to insert a queue object
func StoreQueue(context *gin.Context) {
	var queue *models.Queue
	db := context.MustGet("mongo").(*mongo.Collection)
	if err := context.ShouldBindJSON(&queue); err != nil {
		context.AbortWithStatus(400)
		return
	}
	changedQueue := services.StoreQueue(db, queue)
	if changedQueue != nil {
		context.JSON(200, changedQueue)
		return
	}

	context.AbortWithStatus(400)
	return
}

// GetQueue is used to retrieve a queue object by its id property: responding to /:id
func GetQueue(context *gin.Context) {
	mongo := context.MustGet("mongo").(*mongo.Collection)
	redis := context.MustGet("redis").(*redis.Client)
	id := context.Param("queueId")
	log.Info(services.GetAllEntries(redis, id))
	queue := services.GetQueue(mongo, id, services.GetAllEntries(redis, id))
	if queue == nil {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, queue)
	return
}

// DeleteQueue is used to update a queue object by its _id property
func DeleteQueue(context *gin.Context) {
	db := context.MustGet("mongo").(*mongo.Collection)
	id := context.Param("queueId")
	err := services.DeleteQueue(db, id)
	if err != nil {
		context.AbortWithStatus(400)
		return
	}
	context.Status(200)
	return
}
