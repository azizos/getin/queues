package api

import (
	"dev.azure.com/happyq/HappyQ/_git/queues/api/common"
	"dev.azure.com/happyq/HappyQ/_git/queues/api/models"
	"dev.azure.com/happyq/HappyQ/_git/queues/api/services"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

var log, _ = common.GetLogModule()

// GetAllEntries is used to join a queue
func GetAllEntries(context *gin.Context) {
	queueID := context.Param("queueId")
	db := context.MustGet("redis").(*redis.Client)
	queuesEntities := services.GetAllEntries(db, queueID)
	if queuesEntities != nil {
		context.JSON(200, queuesEntities)
		return
	}
	context.AbortWithStatus(204)
	return
}

// GetQueueSize is used to join a queue
func GetQueueSize(context *gin.Context) {
	queueID := context.Param("queueId")
	db := context.MustGet("redis").(*redis.Client)
	result := services.GetQueueSize(db, queueID)
	context.JSON(200, result)
	return
}

// JoinQueue is used to join a queue
func JoinQueue(context *gin.Context) {
	var queueEntry *models.QueueEntry
	var score int64
	mongo := context.MustGet("mongo").(*mongo.Collection)
	redis := context.MustGet("redis").(*redis.Client)
	if err := context.ShouldBindJSON(&queueEntry); err != nil {
		context.AbortWithStatus(400)
		return
	}
	if !services.QueueExists(mongo, queueEntry.QueueID) {
		log.Infof("Queue %v does not exist, so no entries can be added", queueEntry.QueueID)
		context.AbortWithStatus(400)
		return

	}
	score = services.GetQueueSize(redis, queueEntry.QueueID)
	queue := services.JoinQueue(redis, queueEntry, float64(score))
	if queue == false {
		context.AbortWithStatus(400)
		return
	}
	context.AbortWithStatus(200)
	return
}

// ExitQueue is used to join a queue
func ExitQueue(context *gin.Context) {
	var queueEntry *models.QueueEntry

	db := context.MustGet("redis").(*redis.Client)
	if err := context.ShouldBindJSON(&queueEntry); err != nil {
		context.AbortWithStatus(400)
		return
	}
	queue := services.ExitQueue(db, queueEntry)
	if queue == false {
		context.AbortWithStatus(400)
		return
	}
	context.AbortWithStatus(200)
	return
}

// GetRankingInQueue is used to return the current position of visitor in the queue
func GetRankingInQueue(context *gin.Context) {
	queueID := context.Param("queueId")
	visitorID := context.Param("visitorId")
	db := context.MustGet("redis").(*redis.Client)
	queueEntry := models.QueueEntry{QueueID: queueID, VisitorID: visitorID}
	result, err := services.GetRankingInQueue(db, &queueEntry)
	if err != nil {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, result)
	return
}
