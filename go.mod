module dev.azure.com/happyq/HappyQ/_git/queues

go 1.13

require (
	github.com/ardanlabs/conf v1.2.1
	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.10.0 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
