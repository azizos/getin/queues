# QUEUES API

## QUEUES API

As a part of the HappyQ application, the queues API, written in Go, is used to view, register or modify queues linked to businesses in the HappyQ application.

### What does this service do?

The service offers several HTTP endpoints (described below) using [Gin](https://github.com/gin-gonic/gin) framework. To persist data, we're using  Mongo (to maange queue objects) and Redis (to join and leave queues / manage queue entries).

#### Available endpoints

| Method | Path           | Description                              |
| ------ | -------------- | ---------------------------------------- |
| GET    | /queues        | Retrieve a list of queues                |
| POST   | /queues        | Add a new queue                          |
| GET    | /queues/[id]   | Retrieve a queue by its __id_ property   |
| DELETE | /queues/[id]   | Delete a queue by its _id property       |


| Method | Path                                    | Description                                  |
| ------ | --------------------------------------- | -------------------------------------------  |
| POST   | /entries                                | Join an existing queue                       |
| DELETE | /entries                                | Leave a queue                                |
| GET    | /entries/[queueId]                      | View all entries in a queue (ordered)        |
| GET    | /entries/[queueId]/count                | View the number of entries in a queue        |
| GET    | /entries/[queueId]/visitors/[visitorId] | View current position (ranking) of a visitor |

#### Postman collection 

*todo*

## Run
To run this service locally, you can use `docker-compose` (recommended) using `docker-compose.yml` file available in this repository. 

Run the command below in this project's root directory and the API is locally available on port 8080 (by default) :)
-`docker-compose up --build --remove-orphans`